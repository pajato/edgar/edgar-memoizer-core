package com.pajato.edgar.memoizer.core.sender

import java.io.IOException
import java.nio.channels.AsynchronousCloseException
import java.nio.channels.ClosedByInterruptException
import java.nio.channels.ClosedChannelException
import java.nio.channels.FileChannel
import java.nio.channels.FileLock
import java.nio.channels.OverlappingFileLockException

internal fun getExclusiveLock(channel: FileChannel): FileLock = channel.getLockResult()

internal fun FileChannel.getLockResult(): FileLock =
    try { tryLock() } catch (exc: Exception) { throw getLockException(exc) } ?: throw LockAlreadyHeldException()

internal fun getLockException(exc: Exception): Exception = when (exc) {
    is ClosedByInterruptException -> LockFailInterruptedException()
    is AsynchronousCloseException -> LockFailAsyncClosedException()
    is ClosedChannelException -> LockFailChannelClosedException()
    is OverlappingFileLockException -> LockFailOverlappingException()
    is IOException -> LockFailIOErrorException()
    else -> LockFailUnexpected(exc.message)
}
