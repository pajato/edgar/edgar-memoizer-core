package com.pajato.edgar.memoizer.core

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class StateTest : BaseTest() {

    @Test fun `When the Memoizer is running, verify a positive pid`() {
        assertTrue(State.Running.getPid() > 0)
    }

    @Test fun `When the Memoizer has exited, verify a correct pid`() {
        assertEquals(-1L, State.Exited.getPid())
    }

    @Test fun `When the Memoizer has aborted, verify a correct pid`() {
        assertEquals(-2L, State.Aborted.getPid())
    }

    @Test fun `When the Memoizer has been killed, verify a correct pid`() {
        assertEquals(-3L, State.Killed.getPid())
    }

    @Test fun `When the Memoizer is stale, verify a correct pid`() {
        assertEquals(-4L, State.Stale.getPid())
    }
}
