package com.pajato.edgar.memoizer.core

import com.pajato.edgar.memoizer.core.launcher.Launcher
import com.pajato.edgar.memoizer.core.launcher.NoReasonMessage
import com.pajato.edgar.memoizer.core.launcher.getStartError
import com.pajato.edgar.memoizer.core.launcher.launch
import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class LauncherTest : BaseTest() {
    private val echoResult = File(Config.configBaseDir, "echoResult.txt").also { it.deleteOnExit() }
    private val echoScript = File(Config.configBaseDir, "echoScript.sh").also { it.deleteOnExit() }
    private val echoCommand = listOf("bash", echoScript.absolutePath)

    @Test fun `When a launcher is constructed, verify the process property is uninitialized`() {
        val launcher = Launcher()
        assertFalse(launcher.isProcessInitialized())
    }

    @Test fun `When a launcher command is started without extra args, verify the process property is initialized`() {
        val launcher = Launcher(echoCommand)
        launcher.launch()
        assertTrue(launcher.isProcessInitialized())
        launcher.process.waitFor()
    }

    @Test fun `When a launcher command is started with extra args, verify the process property is initialized`() {
        val launcher = Launcher(echoCommand)
        launcher.launch("; ls >> $echoResult")
        assertTrue(launcher.isProcessInitialized())
        launcher.process.waitFor()
    }

    @Test fun `When an empty command is launched, verify a failed launch`() {
        val command = listOf<String>()

        Config.registerLauncher(Launcher(command))
        assertFalse(memoizerIsRunning())
        assertFailsWith<LaunchFailEmptyCommandException> { launch() }
    }

    @Test fun `When an invalid command is launched, verify a failed launch`() {
        val command = listOf("noSuchCommand", "Hello, World")

        Config.registerLauncher(Launcher(command))
        assertFailsWith<LaunchFailStartError> { launch() }
    }

    @Test fun `When a valid test command is launched, verify the launch result`() {
        if (!echoResult.exists()) echoResult.createNewFile()
        if (!echoScript.exists()) echoScript.createNewFile().also {
            echoScript.writeText("#!/bin/bash\necho \"Hello, World\" > ${echoResult.absolutePath}\n")
        }
        Config.registerLauncher(Launcher(echoCommand))
        assertTrue(memoizerIsNotRunning())
        echoResult.writeText("")
        launch()
        assertTrue(Config.launcher.process.pid() > 0L)
        Config.launcher.process.waitFor()
        assertTrue(echoResult.length() > 0)
    }

    @Test fun `When passing a null exception message, verify launch fail start error exception is correct`() {
        val expected = NoReasonMessage
        assertEquals(expected, getStartError(NullMessageException(null)).message)
    }

    internal class NullMessageException(message: String?) : Exception(message)
}
